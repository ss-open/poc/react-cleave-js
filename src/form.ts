import Cleave from 'cleave.js';

export function setupForm(element: HTMLButtonElement) {
  const minValue = 1;
  const maxValue = 42;

  const inputFormControl = element.querySelector('.form-control-number');
  if (!(inputFormControl instanceof HTMLElement)) {
    console.error('inputFormControl is not a valid HTML element');
    return;
  }

  const inputNumber = inputFormControl.querySelector('input');
  if (!(inputNumber instanceof HTMLElement)) {
    console.error('inputNumber is not a valid HTML element');
    return;
  }

  const helpField = inputFormControl.querySelector('.help');
  if (!(helpField instanceof HTMLElement)) {
    console.error('inputNumber is not a valid HTML element');
    return;
  }

  // const element = document.getElementById('Gd_....');
  // const element = document.querySelector('input[gdln="ta-reference"]');

  // const value = element.value;

  const cleave = new Cleave(inputNumber, { 
    numeral: true, 
    numeralPositiveOnly: true, 
    numeralDecimalScale: 0, 
    numeralThousandsGroupStyle: "none", 
    onValueChanged: function (e) { 
      console.log(
        e.target,
      ); 
    }
    }
  );
}
